`use strict`;

const { INVALID_LINK } = require(`./constants`);
const httpLinkPreview = require(`./httpLinkPreview`);
const httpsLinkPreview = require(`./httpsLinkPreview`);

module.exports = {
    linkPreview: (link) =>
        new Promise((resolve, reject) =>
            getLinkPreview(link)
                .then((response) => resolve(response))
                .catch((catchErr) => reject(catchErr))
        ),
    linkPreviewCallback: (link, callback) =>
        getLinkPreview(link)
            .then((response) => callback(null, response))
            .catch((catchErr) => callback(catchErr, null)),
};

const getLinkPreview = (link) =>
    new Promise((resolve, reject) => {
        if (typeof link != `string` || !link.trim()) {
            return reject(new Error(INVALID_LINK));
        }
        !link.startsWith(`http://`) && !link.startsWith(`https://`)
            ? (link = `http://${link}`)
            : ``;
        link.startsWith(`https`)
            ? httpsLinkPreview(link)
                  .then((response) => resolve(response))
                  .catch((catchErr) => reject(catchErr))
            : httpLinkPreview(link)
                  .then((response) => resolve(response))
                  .catch((catchErr) => reject(catchErr));
    });
