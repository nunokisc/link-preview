`use strict`;

const { getRequest } = require(`./request`);
const { CERTIFICATE_ERROR } = require(`./constants`);
const metaData = require(`./metadata`);

module.exports = (link) =>
    new Promise((resolve, reject) =>
        getRequest(link)
            .then((html) => {
                const dataToSend = metaData(html);
                dataToSend.link = dataToSend.link || link;
                resolve(dataToSend);
            })
            .catch((error) => {
                if (error.message.startsWith(CERTIFICATE_ERROR)) {
                    link = link.replace(`https`, `http`);
                    getRequest(link)
                        .then((html2) => {
                            const dataToSend = metaData(html2);
                            dataToSend.link = dataToSend.link || link;
                            resolve(dataToSend);
                        })
                        .catch((error2) => reject(error2));
                } else {
                    reject(error);
                }
            })
    );
