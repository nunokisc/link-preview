`use strict`;

module.exports = {
    INVALID_LINK: `Your link must be a valid string.`,
    CERTIFICATE_ERROR: `Hostname/IP does not match certificate's altnames:`,
    APPLICATION_CONTROL_VIOLATION: `Application Control Violation`,
    WEB_FILTER_VIOLATION: `Web Filter Violation`,
};
