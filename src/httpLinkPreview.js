`use strict`;

const { getRequest } = require(`./request`);
const {
    APPLICATION_CONTROL_VIOLATION,
    WEB_FILTER_VIOLATION,
} = require(`./constants`);
const metaData = require(`./metadata`);

module.exports = (link) =>
    new Promise((resolve, reject) =>
        getRequest(link)
            .then((html) => {
                const dataObtained = metaData(html);
                if (
                    dataObtained.title == APPLICATION_CONTROL_VIOLATION ||
                    dataObtained.title == WEB_FILTER_VIOLATION
                ) {
                    link = link.replace(`http`, `https`);
                    getRequest(link)
                        .then((html2) => {
                            const dataToSend = metaData(html2);
                            dataToSend.link = dataToSend.link || link;
                            resolve(dataToSend);
                        })
                        .catch((error2) => reject(error2));
                } else {
                    dataObtained.link = dataObtained.link || link;
                    resolve(dataObtained);
                }
            })
            .catch((error) => reject(error))
    );
