`use strict`;

const axios = require(`axios`);

module.exports = {
    getRequest: (link) =>
        new Promise((resolve, reject) =>
            axios
                .get(link,{ headers: { 'User-Agent': 'facebookexternalhit/1.1' }  })
                .then((response) => resolve(response.data))
                .catch((catchErr) => reject(catchErr))
        ),
};
